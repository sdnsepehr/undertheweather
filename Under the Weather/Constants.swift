//
//  Constants.swift
//  Under the Weather
//
//  Created by Saifuddin Sepehr on 6/7/17.
//  Copyright © 2017 dvt. All rights reserved.
//

import UIKit

struct Colors {
    static let viewBackground = UIColor(r: 40, g: 156, b: 220);
    static let titleBackground = UIColor(r: 17, g: 140, b: 208);
    static let addressTextColor = UIColor.black;
    static let otherTextsColor = UIColor(r: 253, g: 253, b: 253);
}
