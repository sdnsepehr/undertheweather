//
//  WeatherModel.swift
//  Under the Weather
//
//  Created by Saifuddin Sepehr on 6/8/17.
//  Copyright © 2017 dvt. All rights reserved.
//

import Foundation
import Alamofire

class WeatherModel {
    private var _timeInterval: TimeInterval?;
    private var _maxTemprature: String?
    private var _minTemprature: String?
    private var _location: String?
    private var _currentTemp: String?
    private var _weatherIcon: String?
    typealias JSON = Dictionary<String, Any>
    
    var date: String {
        let dateFormatter = DateFormatter();
        dateFormatter.dateStyle = .long;
        dateFormatter.timeStyle = .none;
        let date = Date(timeIntervalSince1970: _timeInterval!);
        
        let dateString = (_timeInterval != nil) ? "Today, \(dateFormatter.string(from: date))" : "Invalid Date";
        
        return dateString.uppercased()
    }
    
    var maxTemp: String {
        return "max \(_maxTemprature ?? "0" )";
    }
    
    var minTemp: String {
        return "min \(_minTemprature ?? "0" )";
    }
    
    var currentTemp: String {
        return "now \(_currentTemp ?? "0" )";
    }
    
    var location: String {
        return _location ?? "Location Invalid";
    }
    
    var weatherIcon: String {
        return _weatherIcon ?? "other";
    }
    
    func downloadData(cityName: String, completed: @escaping ()-> ()) {
         let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(cityName)&appid=edc1105a80124d9002354a56e5af3cde")!
        
        print("Downloading data...");
        
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            let result = response.result
            if let dict = result.value as? JSON, let main = dict["main"] as? JSON, let currentTemp = main["temp"] as? Double, let maxTemp = main["temp_max"]  as? Double, let minTemp = main["temp_max"]  as? Double, let weatherArray = dict["weather"] as? [JSON], let icon = weatherArray[0]["icon"] as? String, let name = dict["name"] as? String, let sys = dict["sys"] as? JSON, let country = sys["country"] as? String, let dt = dict["dt"] as? Double {
                
                print("Icon: ", icon)
//                print(dict["icon"]);
                
                self._currentTemp = String(format: "%.0f °C", currentTemp - 273.15);
                self._maxTemprature = String(format: "%.0f °C", maxTemp - 273.15);
                self._minTemprature = String(format: "%.0f °C", minTemp - 273.15);
                self._weatherIcon = icon
                self._location = "\(name), \(country)"
                self._timeInterval = dt;
            }
            
            completed()
        })
    }
    

    
    
}
