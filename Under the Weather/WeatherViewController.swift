//
//  ViewController.swift
//  Under the Weather
//
//  Created by Saifuddin Sepehr on 6/7/17.
//  Copyright © 2017 dvt. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class WeatherViewController: UIViewController, CLLocationManagerDelegate {
    
    var weatherData = WeatherModel();
    let locationManager = CLLocationManager()
    
    let todayDateLabel: UILabel = {
        let label = UILabel();
        label.font = UIFont.boldSystemFont(ofSize: 20);
        label.textAlignment = .center;
        label.textColor = Colors.otherTextsColor;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    let weatherStateImageView: UIImageView = {
        let imageView = UIImageView();
        imageView.contentMode = .scaleToFill;
        imageView.layer.masksToBounds = true;
        imageView.translatesAutoresizingMaskIntoConstraints = false;
        return imageView;
    }()
    
    let currentDegreeLabel: UILabel = {
        let label = UILabel();
        label.font = UIFont.boldSystemFont(ofSize: 24);
        label.textColor = Colors.otherTextsColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    let maxDegreeLabel: UILabel = {
        let label = UILabel();
        label.font = UIFont.boldSystemFont(ofSize: 24);
        label.textColor = Colors.otherTextsColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    let minDegreeLabel: UILabel = {
        let label = UILabel();
        label.font = UIFont.boldSystemFont(ofSize: 14);
        label.textColor = Colors.otherTextsColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    let addressLabel: UILabel = {
        let label = UILabel();
        label.font = UIFont.boldSystemFont(ofSize: 14);
        label.textColor = Colors.addressTextColor;
        label.textAlignment = .center;
        label.translatesAutoresizingMaskIntoConstraints = false;
        return label;
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.viewBackground;
        title = "Under the Weather";
        
        view.addSubview(todayDateLabel);
        view.addSubview(weatherStateImageView);
        view.addSubview(currentDegreeLabel);
        view.addSubview(maxDegreeLabel);
        view.addSubview(minDegreeLabel);
        view.addSubview(addressLabel);
        
         setupConstraints()
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.requestWhenInUseAuthorization();
        locationManager.startUpdatingLocation();
    }
    
    private func setupConstraints(){
        
        // Constraints for todayDateLabel
        todayDateLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.size.height/6).isActive = true;
        todayDateLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        todayDateLabel.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/12).isActive = true;
        todayDateLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.95).isActive = true;
        
        // Constraints for weatherStateImageView
        weatherStateImageView.topAnchor.constraint(equalTo: todayDateLabel.bottomAnchor, constant: view.frame.size.height/18).isActive = true;
        weatherStateImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        weatherStateImageView.widthAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/6).isActive = true;
        weatherStateImageView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/6).isActive = true;
        
        // Constraints for currentDegreeLable
        currentDegreeLabel.topAnchor.constraint(equalTo: weatherStateImageView.bottomAnchor, constant: view.frame.size.height/40).isActive = true;
        currentDegreeLabel.widthAnchor.constraint(equalTo: todayDateLabel.widthAnchor).isActive = true;
        currentDegreeLabel.heightAnchor.constraint(equalTo: todayDateLabel.heightAnchor).isActive = true;
        currentDegreeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        // Constraints for maxDegreeLabel
        maxDegreeLabel.topAnchor.constraint(equalTo: currentDegreeLabel.bottomAnchor, constant: view.frame.size.height/35).isActive = true;
        maxDegreeLabel.widthAnchor.constraint(equalTo: todayDateLabel.widthAnchor).isActive = true;
        maxDegreeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        // Constraints for minDegreeLabel
        minDegreeLabel.topAnchor.constraint(equalTo: maxDegreeLabel.bottomAnchor, constant: view.frame.size.height/70).isActive = true;
        minDegreeLabel.widthAnchor.constraint(equalTo: todayDateLabel.widthAnchor).isActive = true;
        minDegreeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
        // Constraints for addressLabel
        addressLabel.topAnchor.constraint(equalTo: minDegreeLabel.bottomAnchor, constant: view.frame.size.height/20).isActive = true;
        addressLabel.widthAnchor.constraint(equalTo: todayDateLabel.widthAnchor).isActive = true;
        addressLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true;
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            if (error != nil) {
                print(error!)
                return;
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks?[0]
                self.displayLocationInfo(placemark: pm);
            } else {
                print("Problem with the data recived from geo");
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//            print("Failed with errror, ", error.localizedDescription);
    }
    
    func displayLocationInfo(placemark: CLPlacemark?) {
        if placemark != nil {
            
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            if let cityName = placemark?.locality{
                print("geting weather data for ", cityName);
                self.weatherData.downloadData(cityName: cityName) {
                    print("Data Donwload Completed");
                    self.updateUI();
                }
            }
        }
    }
    
    private func updateUI(){
        todayDateLabel.text = weatherData.date;
        
        print("Date: ", weatherData.date);
        
        if let weatherImage = UIImage(named: weatherData.weatherIcon) {
            weatherStateImageView.image = weatherImage;
        } else {
            weatherStateImageView.image = #imageLiteral(resourceName: "other");
        }
        
        currentDegreeLabel.text = weatherData.currentTemp;
        maxDegreeLabel.text = weatherData.maxTemp;
        minDegreeLabel.text = weatherData.minTemp;
        addressLabel.text = weatherData.location;
    }
    
}

